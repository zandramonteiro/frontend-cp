import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firebase-firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAqE-xCrgkg5rDcmOi5tKKOmEbjumDDU4s",
    authDomain: "cp-front.firebaseapp.com",
    databaseURL: "https://cp-front.firebaseio.com",
    projectId: "cp-front",
    storageBucket: "cp-front.appspot.com",
    messagingSenderId: "607253522526",
    appId: "1:607253522526:web:138650b7087f787306accf"
  };



  class Firebase{
    constructor(){
      app.initializeApp(firebaseConfig);
      this.auth = app.auth();
      this.db = app.firestore();
    }

    login(email, password){
      return this.auth.signInWithEmailAndPassword(email, password);
    }

    logout(){
      return this.auth.signOut();
    }

    async register (name, email, password){
      await this.auth.createUserWithEmailAndPassword(email, password);
      return this.auth.currentUser.updateProfile({
        displayName: name
      })
    }

    addQuote(quote){
      if(!this.auth.currentUser){
        return alert("Não autorizado")
      }

      return this.db.doc(`users_claimportal/${this.auth.currentUser.uid}`).set({
        quote
      })
    }

    isInitialized() {
      return new Promise(resolve => {
        this.auth.onAuthStateChanged(resolve)
      })
    }
  
    getCurrentUsername() {
      return this.auth.currentUser && this.auth.currentUser.displayName
    }
  
    async getCurrentUserQuote() {
      const quote = await this.db.doc(`users_claimportal/${this.auth.currentUser.uid}`).get()
      return quote.get('quote')
    }
  }

  export default new Firebase();